# Exercício 4

class controlarDesconto(object):

    def __init__(self, preco):
        self.preco_produto = 0
        self.__porcentagem_desconto = 0

    @property
    def porcentagem(self):
        # print(f'Porcentagem de desconto: {int(self.__porcentagem_desconto)}')
        # print(f'Preço com desconto aplicado: {self.preco_produto}')
        return(self.__porcentagem_desconto)

    @porcentagem.setter
    def porcentagem(self, val_porcent):
        if val_porcent < 1:
            self.__porcentagem_desconto = 1
            self.preco_produto -= 0.01*self.preco_produto

        elif val_porcent > 35:
            self.__porcentagem_desconto = 1
            self.preco_produto -= 0.01 * self.preco_produto

        else:
            self.__porcentagem_desconto = val_porcent
            self.preco_produto -= (val_porcent/100) * self.preco_produto


preco = float(input('Entre com o valor do produto: '))
p_desconto = int(input('Entre com o percentual de desconto: '))

desconto = controlarDesconto(preco)

desconto.porcentagem(p_desconto)

# print(desconto.porcentagem)