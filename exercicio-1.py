# Exercício 1

pessoa = None

class Homem(object):

    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade

    def escolha_homem(self):
        print('Pessoa criada, esta pessoa é um homem!')


class Mulher(object):
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade

    def escolha_mulher(self):
        print('Pessoa criada, esta pessoa é uma mulher!')


print('Escolha a opção:')
choice = int(input('1 - Homem ou 2 - Mulher: '))

if choice == 1:
    pessoa = Homem(nome='nome1', idade=30)
    pessoa.escolha_homem()

elif choice == 2:
    pessoa = Mulher(nome='nome2', idade=40)
    pessoa.escolha_mulher()
else:
    print('Opção inválida!')