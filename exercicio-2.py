# Exercício 2

class Conta(object):

    def __init__(self, numero, nome):
        self.numero = numero
        self.nome = nome
        self.saldo = 0

    def alterar_nome(self, nome):
        self.nome = nome
        print(f'Nome alterado para: {self.nome}!')

    def depositar(self, valor):
        self.saldo += valor
        print('Deposito realizado com sucesso!')
        print(f'Novo saldo: {self.saldo}!')

    def sacar(self, valor):
        self.saldo -= valor
        print('Saque realizado com sucesso!')
        print(f'Novo saldo: {self.saldo}!')

print('Crie sua conta: ')
numero_conta = int(input('Digite um numero para a conta: '))
nome_cliente = input('Digite seu nome: ')

conta1 = Conta(numero=numero_conta, nome=nome_cliente)

print('Conta criada!')

while True:
    print('Opções:')
    print('1 - Alterar nome')
    print('2 - Depositar')
    print('3 - Sacar')
    print('4 - Sair')
    choice = int(input('Escolha a opção que deseja:'))

    if choice == 1:
        nome = input('Entre com o novo nome: ')
        conta1.alterar_nome(nome)

    elif choice == 2:
        valor = float(input('Entre com o valor: '))
        conta1.depositar(valor)

    elif choice == 3:
        valor = float(input('Entre com o valor: '))
        conta1.sacar(valor)

    elif choice == 4:
        break

    else:
        print('Opção Inválida!')


