class calculaImposto(object):
    def __init__(self):
        self.__taxa = 0

    @property
    def taxa(self):
        return round(self.__taxa, 2)

    @taxa.setter
    def taxa(self, val_taxa):
        if val_taxa < 0:
            self.__taxa = 0

        elif val_taxa > 100:
            self.__taxa = 100

        else:
            self.__taxa = val_taxa