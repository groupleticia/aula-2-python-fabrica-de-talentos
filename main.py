# class Aluno(object):
#
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#         self.nota_1 = 0
#         self.nota_2 = 0
#
#     def addNotas(self, nota_1, nota_2):
#         self.nota_1 = nota_1
#         self.nota_2 = nota_2
#
#     def media(self):
#         return ((self.nota_1 + self.nota_2)/2)
#
#     def situacao(self):
#         media = self.media()
#         if media < 40:
#             print("Reprovado!")
#         elif media >= 60:
#             print("Aprovado!")
#         else:
#             print("NP3!")
#
#
# # aluno_1 = Aluno(nome="Leandro", idade="22")
# # print(aluno_1.nome, aluno_1.idade)
#
# aluno_2 = Aluno(nome="João", idade=25)
# aluno_2.addNotas(nota_1=10, nota_2=50)
# # print(aluno_2.nome, aluno_2.idade, aluno_2.media())
#
# # aluno_2.situacao()
#
# print(dir(Aluno))


# class Animal(object):
#     pass
#
# class Ave(Animal):
#     pass
#
# class BeijaFlor(Ave):
#     pass

# class Pai(object):
#     nome = 'Carlos'
#     sobrenome = 'Oliveira'
#     residencia = 'Santa Rita'
#     olhos = 'azuis'
#
# class Filha(Pai):
#     nome = 'Luciana'
#     olhos = 'castanhos'
#
# class Neta(Filha):
#     nome = 'Maria'
#
# print(Pai.nome, Filha.nome, Neta.nome)
# print(Pai.residencia, Filha.residencia, Neta.residencia)
# print(Pai.olhos, Filha.olhos, Neta.olhos)

# class Pessoa(object):
#     nome = None
#     idade = None
#
#     def __init__(self, nome, idade):
#         self.nome = nome
#         self.idade = idade
#
#     def envelhecer(self):
#         self.idade += 1
#
# class Atleta(Pessoa):
#     peso = None
#     aposentado = None
#
#     def __init__(self, nome, idade, peso):
#         super().__init__(nome,idade)
#         self.peso = peso
#
#     def aquecer(self):
#         print('Atleta aquecido!')
#
#     def aposentar(self):
#         self.aposentado = True
#
# class Corredor(Atleta):
#     def correr(self):
#         print('Correndo!!')
#
# class Nadador(Atleta):
#     def nadar(self):
#         print('Nadando!!')
#
# class Ciclista(Atleta):
#     def pedalar(self):
#         print('Pedalando!!')
#
# corredor_1 = Corredor(nome='Leandro', idade= 25, peso=80)
# nadador_1 = Nadador(nome='João', idade= 30, peso=75)
# ciclista_1 = Ciclista(nome='Pedro', idade= 21, peso=60)
#
# corredor_1.aquecer()
# corredor_1.correr()

# def decorador(func):
#     def wrapper():
#         print('Antes de executar')
#         func()
#         print('Depois de executar')
#     return wrapper
#
# @decorador
# def media_da_turma():
#     nota_1 = 10
#     nota_2 = 5
#     print(f'Média = {(nota_1 + nota_2)/2}')
#
# media_da_turma()

from classes import calculaImposto

calc = calculaImposto()

calc.taxa = 105
print(calc.taxa)


